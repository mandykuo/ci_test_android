package com.ci.example;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainActivityPresenterTest {
    @Test
    public void testAdd() {
        MainActivityPresenter presenter = new MainActivityPresenter();
        assertEquals(6, presenter.add(3, 3));
    }
}