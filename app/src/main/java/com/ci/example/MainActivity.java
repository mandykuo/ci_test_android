package com.ci.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainActivityPresenter presenter = new MainActivityPresenter();
        int ans = presenter.add(2, 2);
        TextView textView = findViewById(R.id.anwser);
        textView.setText(String.valueOf(ans));
    }
}
